package tbot

type ChosenInlineResult struct {
	ResultId 	string	`json:"result_id"`
	From 		User 	`json:"from"`
	InlineMessageId string 	`json:"inline_message_id"`
	Query 		string 	`json:"query"`
}
