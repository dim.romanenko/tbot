package tbot

import "encoding/json"

type InlineKeyboardMarkup struct {
	InlineKeyboard [][]InlineKeyboardButton `json:"inline_keyboard"`
}

type inlineKeyboardMarkupJSON InlineKeyboardMarkup

func (rm InlineKeyboardMarkup) MarshalJSON() ([]byte, error) {
	k := inlineKeyboardMarkupJSON{
		InlineKeyboard: rm.InlineKeyboard,
	}

	return json.Marshal(k)
}