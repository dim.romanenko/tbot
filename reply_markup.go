package tbot

import "encoding/json"

type ReplyMarkup interface {
	json.Marshaler
}
