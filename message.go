package tbot

type Message struct {
	MessageId 	int		`json:"message_id"`
	From 		User 		`json:"from"`
	Text 		string 		`json:"text"`
	Date 		int 		`json:"date"`
	Chat 		Chat 		`json:"chat"`
	ForwardFrom 	User		`json:"forward_from"`
	ForwardFromChat Chat 		`json:"forward_from_chat"`
	ReplyToMessage 	*Message 	`json:"reply_to_message"`
	EditDate 	int 		`json:"edit_date"`
	Entities	[]MessageEntity `json:"entities"`
}

func (m *Message) IsBotCommand() bool {
	for _, e := range m.Entities {
		if e.Type == BotCommand {
			return true
		}
	}

	return false
}