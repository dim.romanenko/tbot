package tbot

import "encoding/json"

type ReplyKeyboardMarkup struct {
	Keyboard 	[][]KeyboardButton 	`json:"keyboard"`
	ResizeKeyboard 	bool 			`json:"resize_keyboard"`
	OneTimeKeyboard bool 			`json:"one_time_keyboard"`
	Selective 	bool 			`json:"selective"`
}

type replyKeyboardMarkupJSON ReplyKeyboardMarkup

func (rm ReplyKeyboardMarkup) MarshalJSON() ([]byte, error) {
	k := replyKeyboardMarkupJSON{
		Keyboard: 		rm.Keyboard,
		ResizeKeyboard: 	rm.ResizeKeyboard,
		OneTimeKeyboard: 	rm.OneTimeKeyboard,
		Selective: 		rm.Selective,
	}

	return json.Marshal(k)
}
