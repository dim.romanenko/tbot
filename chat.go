package tbot

type Chat struct {
	Id 		int
	Type 		string
	Title 		string
	Username 	string
	FirstName 	string `json:"first_name"`
	LastName 	string `json:"last_name"`
}
